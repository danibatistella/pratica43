
import utfpr.ct.dainf.if62c.pratica.Retangulo;
import utfpr.ct.dainf.if62c.pratica.Quadrado;
import utfpr.ct.dainf.if62c.pratica.TrianguloEquilatero;
import utfpr.ct.dainf.if62c.pratica.Elipse;
import utfpr.ct.dainf.if62c.pratica.Circulo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Danielli Batistella
 */
public class Pratica43 {

    public static void main(String[] args) {
        Elipse elipse = new Elipse(2, 4);
        double area = elipse.getArea();
        double perimetro = elipse.getPerimetro();
        Circulo circulo = new Circulo(10, 2, 4);
        double Carea = circulo.getArea();
        double Cperimetro = circulo.getPerimetro();
        Quadrado quadrado = new Quadrado(4);
        double Qarea = quadrado.getArea();
        double Qperimetro = quadrado.getPerimetro();
        Retangulo retangulo = new Retangulo(4, 8);
        double Rarea = retangulo.getArea();
        double Rperimetro = retangulo.getPerimetro();
        TrianguloEquilatero triangulo = new TrianguloEquilatero(6, 8, 10);
        double Tarea = triangulo.getArea();
        double Tperimetro = triangulo.getPerimetro();
    }

}
