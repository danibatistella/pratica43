package utfpr.ct.dainf.if62c.pratica;


import utfpr.ct.dainf.if62c.pratica.FiguraComLados;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Danielli Batistella
 */
public class Retangulo implements FiguraComLados {
    
    double lado1; //variáveis
    double lado2;
    
     public Retangulo(double L1, double L2) // construtor
    {
     lado1 = L1;
     lado2 = L2;
     }
       @Override 
     public double getLadoMenor()
    {   
     return 0;
    }

       @Override
     public double getLadoMaior() 
    {
      return 0;
    }
    public double getArea()
    { double AR=lado1*lado2;
        return AR;
    }
     public double getPerimetro()
    { double PR=lado1 + lado2 + lado1 + lado2;
        return PR;
    }
}
