package utfpr.ct.dainf.if62c.pratica;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import static java.lang.Math.PI;

    public class Elipse implements FiguraComEixos {  
    double r;  //variaveis
    double s;
    
    public Elipse(double pr, double ps) // construtor
    {
     r = pr;
     s = ps;
    }
     @Override
    public double getEixoMenor()
    {   
     return 0;
    }
    
    @Override
    public double getEixoMaior() 
    {
      return 0;
    }
    @Override
    public double getArea()
    { double A=PI*r*s;
        return A;
    }
    
    @Override
    public double getPerimetro()
    {
        double P = (PI*(3*(r + s))- (Math.sqrt((3*r+s)*(r+3*s))));
        return P;
    }
 }