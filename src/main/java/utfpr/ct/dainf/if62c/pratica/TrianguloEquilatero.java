package utfpr.ct.dainf.if62c.pratica;


import utfpr.ct.dainf.if62c.pratica.FiguraComLados;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Danielli Batistella
 */
public class TrianguloEquilatero implements FiguraComLados {
    double base;
    double altura;
    double lado;
    
    public TrianguloEquilatero(double ba, double al, double la) // construtor
    {
        base = ba;
        altura = al;
        lado = la;
    }
     @Override 
     public double getLadoMenor()
    {   
     return 0;
    }

       @Override
     public double getLadoMaior() 
    {
      return 0;
    }
    public double getArea()
    {
     double TA=(base*altura)/2;
        return TA;
    }
    public double getPerimetro()
    {
     double TP= lado+lado+lado;
        return TP;
    }
}
    
