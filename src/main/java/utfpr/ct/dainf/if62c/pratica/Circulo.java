package utfpr.ct.dainf.if62c.pratica;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import static java.lang.Math.PI;

/**
 *
 * @author Danielli Batistella
 */
public class Circulo extends Elipse { 
    
    double raio;  //variaveis

    public Circulo(double pr, double ps, double praio) {
        super(pr, ps);
        raio = praio;
    }

 

   
    @Override
    public double getPerimetro()
    {
        double PC = 2*PI*raio;
        return PC;
     }
}